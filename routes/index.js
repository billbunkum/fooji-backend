var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Simple API' });
});

// code goes below here
var mongoose = require('mongoose');
var TodoItem = mongoose.model('TodoItem');

// PRELOAD - uses param() and 'query interface' to preload TodoItem
// a route URL w/ :todoItem in it will automatically run this function and attach the found 'todoItem' object to the 'req' object.
router.param('todoItem', function(req, res, next, id) {
    var query = TodoItem.findById(id);

    query.exec(function (err, todoItem) {
        if(err) { return next(err); }
        if(!todoItem) { return next(new Error('can\'t find the to-do-item')); }

        req.todoItem = todoItem;
        return next();
    });
});

// GET all todoItems
router.get('/lists', function(req, res, next) {
    TodoItem.find(function (err, todoItems) {
        if(err) { return next(err); }

        res.json(todoItems);
    });
});

// GET solitary todoItem - testing preloader
router.get('/lists/:todoItem', function(req, res) {
    res.json(req.todoItem);
});

// 'markComplete' method on TodoItem docs
router.put('/lists/:todoItem/markComplete', function(req, res, next) {
    req.todoItem.markComplete(function(err, todoItem) {
        if(err) { return next(err); }

        res.json(todoItem);
    });
});

// 'markIncomplete' method on TodoItem docs
router.put('/lists/:todoItem/markIncomplete', function(req, res, next) {
    req.todoItem.markIncomplete(function(err, todoItem) {
        if(err) { return next(err); }

        res.json(todoItem);
    });
});

// POST - create a todo item
router.post('/lists', function(req, res, next) {
    var todoItem = new TodoItem(req.body);

    todoItem.save(function (err, todoItem) {
        if(err) { return next(err); }

        res.json(todoItem);
    });
});

// DELETE solitary todoItem
router.delete('/lists/:todoItem', function(req, res, next) {
    mongoose.model('TodoItem').findById(req.todoItem.id, function(err, todoItem) {
        if(err) { return next(err); }

        todoItem.remove(function(err, todoItem) {
            if(err) { return next(err); }

            res.format({
                // go back to HOME page '/'
                html: function(){
                    res.redirect('/');
                },
                json: function(){
                    res.json({
                        message: req.todoItem.name + ' deleted. No goin\' back, bub',
                        item: todoItem
                    });
                }
            });
        });
    });
});

// code goes above here
module.exports = router;