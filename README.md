# Fooji-dos -- the backend!

## An API To-Do list
+ The user should be able to:
    1. CRUD
    2. Group into 'lists' Completed or Not-Completed
+ Uses MEAN currently w/o AngularJS added - must add yo'sef

## DIR setup
+ app.js - launches app, imports server files, configure routes, opens db connections, etc.
+ bin/ - container execs. www launches app.js starts Node.js server
+ public/ - .js, .css, static files and templates
+ routes/ - Node controllers and backend code
+ views/ - using .ejs instead of .jade.
+ models/ - contains Mongoose scheme definitions

## Local setup
+ using mkvirtualenv fooji-dos
+ using nodeenv fooji-backend
+ app dir is: fooji-dos/
+ make sure mongodb is running/stopped w/ `$ sudo service mongod start/stop`
+ start server w/ `$ npm start` and listen on port 3000`

## ROUTES
+ GET / POST `localhost:3000/lists`
+ GET / PUT / DELETE `localhost:3000/lists/< _ID >`
+ Model known as `TodoItem`
+ Document "model instance" known as `todoItem`
+ each todoItem has properties: 
    1. name - `String` 
    2. completion - `Boolean` defaults to `false`
    3. date - `Date.now`

### GROUPING todoItems into LISTS
+ Grouping intended for Front-end. Use AngularJS GET requests to query database for `completion` properties
+ While simple, a 'completed' todo item list and an 'incomplete' todo item list can be got using this usage.
+ To grab a list of 'completed' items, `GET /lists/` and then filter by `todoItem.completion == true`. Use `false` for a list of incomplete items.

## TESTing routes w/ cURL
+ POST an entry - `$ curl --data 'name=< foo >' http://localhost:3000/lists`
+ GET all entries - `$ curl -G http://localhost:3000/lists`
+ GET a solitary entry - `$ curl -G http://localhost:3000/lists/< _ID > `
+ DELETE an entry - `$ curl -X DELETE http://localhost:3000/lists/< _ID >
+ PUT methods `markComplete` and `markIncomplete1` - `$ curl -X PUT http://localhost:3000/lists/< _ID >/<method markComplete / markIncomplete >`