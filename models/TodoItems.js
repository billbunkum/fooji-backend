var mongoose = require("mongoose");

var TodoItemSchema = new mongoose.Schema({
    name: String,
    completion: { 
        type: Boolean, 
        default: false 
    }, //will use to group to-do-items into completed/not-completed lists
    date: { 
        type: Date, 
        default: Date.now 
    }
});

// Ternary doesn't work? Fuck'em. Make two methods!
// 'cb' apparently stands for 'call back'
TodoItemSchema.methods.markComplete = function(cb) {
    this.completion = true;
    this.save(cb);
};

TodoItemSchema.methods.markIncomplete = function(cb) {
    this.completion = false;
    this.save(cb);
};

mongoose.model("TodoItem", TodoItemSchema);